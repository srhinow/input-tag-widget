<?php

declare(strict_types=1);

/**
 * Created by casemanager_stage.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 25.05.22
 */

namespace Srhinow\InputTagWidget;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SrhinowInputTagWidgetBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}

