<?php

declare(strict_types=1);

/**
 * Created by casemanager_stage.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 25.05.22
 */

namespace Srhinow\InputTagWidget\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Srhinow\InputTagWidget\SrhinowInputTagWidgetBundle;

class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(SrhinowInputTagWidgetBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}